'use strict';
// fixed svg show
//-----------------------------------------------------------------------------
svg4everybody();

// checking if element for page
//-----------------------------------------------------------------------------------
function isOnPage(selector) {
    return ($(selector).length) ? $(selector) : false;
}
// search page
function pageWidget(pages) {
  var widgetWrap = $('<div class="widget_wrap"><ul class="widget_list"></ul></div>');
  widgetWrap.prependTo("body");
  for (var i = 0; i < pages.length; i++) {
    if (pages[i][0] === '#') {
      $('<li class="widget_item"><a class="widget_link" href="' + pages[i] +'">' + pages[i] + '</a></li>').appendTo('.widget_list');
    } else {
      $('<li class="widget_item"><a class="widget_link" href="' + pages[i] + '.html' + '">' + pages[i] + '</a></li>').appendTo('.widget_list');
    }
  }
  var widgetStilization = $('<style>body {position:relative} .widget_wrap{position:fixed;top:0;left:0;z-index:9999;padding:20px 20px;background:#222;border-bottom-right-radius:10px;-webkit-transition:all .3s ease;transition:all .3s ease;-webkit-transform:translate(-100%,0);-ms-transform:translate(-100%,0);transform:translate(-100%,0)}.widget_wrap:after{content:" ";position:absolute;top:0;left:100%;width:24px;height:24px;background:#222 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAgMAAABinRfyAAAABGdBTUEAALGPC/xhBQAAAAxQTFRF////////AAAA////BQBkwgAAAAN0Uk5TxMMAjAd+zwAAACNJREFUCNdjqP///y/DfyBg+LVq1Xoo8W8/CkFYAmwA0Kg/AFcANT5fe7l4AAAAAElFTkSuQmCC) no-repeat 50% 50%;cursor:pointer}.widget_wrap:hover{-webkit-transform:translate(0,0);-ms-transform:translate(0,0);transform:translate(0,0)}.widget_item{padding:0 0 10px}.widget_link{color:#fff;text-decoration:none;font-size:15px;}.widget_link:hover{text-decoration:underline} </style>');
  widgetStilization.prependTo(".widget_wrap");
}

$(document).ready(function($) {
  pageWidget(['index', 'catalogue', 'product-card', '#recall-modal', 'news', 'post', 'portfolio', 'about', '#call-modal']);
});

$('input[type="tel"]').inputmask();

if($(window).width() > 1199) {
  var wow = new WOW(
    {
    boxClass:     'wow',      // default
    animateClass: 'animated', // default
    offset:       0,          // default
    mobile:       true,       // default
    live:         true        // default
  }
  )
  wow.init();
  new fullpage('#full-page', {
    licenseKey: 'xxxxxxxx-xxxxxxxx-xxxxxxxx-xxxxxxxx',
    // anchors: ['golovna', 'pro_klub', 'poslugy', 'video_360', 'ciny', 'skidka', 'svitlyny', 'yak_nas_znajty'],
    // menu: '#menu',
    slidesNavigation: true,
    scrollBar:true
    // scrollingSpeed: 250,
    // scrollOverflow: true,
    // responsiveWidth: 476,
    // afterResponsive: function(isResponsive){
    //     //console.log(isResponsive);
    // },
    // afterRender: function(){
    //     setInterval(function(){
    //         $.fn.fullpage.moveSlideRight();
    //     }, 5000);
    // }
  })
}

$(document).on('click', '.go-next', function() {
  fullpage_api.moveTo($(this).data('my-anchor'));
});

$(document).on('click', '.menu-button', function() {
  $('.nav').toggleClass('active');
  $('.header-menu').fadeToggle();
});

$('.numbers-slider').slick({
  infinite: false,
  arrows: false,
  dots: false,
  variableWidth: true,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        centerMode: true,
        infinite: true,
      }
    }
  ]
});

$(document).on('click', '.numbers-slider div[data-slick-index]', function () {
    var slide_index = $(this).index();
    $('.numbers-slider').slick('slickGoTo', slide_index);
});

$('.phones-slider').slick({
  arrows: true,
  dots: false,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: true,
      }
    }
  ]
})

$(document).on('click', '.slider-button', function() {
  let thisIndex = $(this).index();

  $('.numbers-slider').slick('slickUnfilter');
  $('.numbers-slider').slick('slickFilter', $(this).data('filter'));
  $('.numbers-slider').slick("slickGoTo", 0);
  $('.slider-button').removeClass('active');
  $('.slider-button').eq(thisIndex).addClass('active');
});

$('.m-main-slider').slick({
  infinite: true,
  dots: true,
  arrows: true,
})

$('.star').on('mouseover', function(){
  var onStar = parseInt($(this).data('value'), 10);

  $(this).parent().children('.star').each(function(e){
    if (e < onStar) {
      $(this).addClass('hover');
    }
    else {
      $(this).removeClass('hover');
    }
  });
  
}).on('mouseout', function(){
  $(this).parent().children('.star').each(function(e){
    $(this).removeClass('hover');
  });
});

$('.star').on('click', function(){
  var onStar = parseInt($(this).data('value'), 10);
  var stars = $(this).parents('.review-stars-row').find('.star');
  
  for (var i = 0; i < stars.length; i++) {
    $(stars[i]).removeClass('selected');
    // $(stars[i]).find('.star-checkbox').removeAttr('checked');
  }
  
  for (var i = 0; i < onStar; i++) {
    $(stars[i]).addClass('selected');
    // $(stars[i]).find('.star-checkbox').attr('checked', 'checked');
  }
});

$('.product-slider').slick({
  infinite: true,
  arrows: false,
  dots: false,
  asNavFor: '.product-sub-slider',
});

$('.product-sub-slider').slick({
  infinite: true,
  arrows: true,
  dots: false,
  slidesToShow: 4,
  slidesToScroll: 1,
  focusOnSelect: true,
  asNavFor: '.product-slider',
  responsive: [
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 3,
      }
    }
  ]
});

$(document).on('click', '.review-form-show', function() {
  $(this).fadeOut();
  setTimeout(function() {
    $('.review-form').fadeIn();
  }, 400);
});

$(document).on('click', '.show-product-text', function() {
  $(this).parents('.product-text-block').toggleClass('show')
});

$('.portfolio-slider').slick({
  infinite: true,
  dots: false,
  arrows: true,
  asNavFor: '.portfolio-sub-slider',
  responsive: [
    {
      breakpoint: 992,
      settings: {
        arrows: false,
      }
    },
  ]
})

$('.portfolio-sub-slider').slick({
  infinite: true,
  dots: false,
  arrows: true,
  slidesToShow: 9,
  slidesToScroll: 1,
  focusOnSelect: true,
  asNavFor: '.portfolio-slider',
  responsive: [
    {
      breakpoint: 1700,
      settings: {
        slidesToShow: 6,
      }
    },
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 5,
      }
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 3,
      }
    },
  ]
})

if ($('body').hasClass('index')) {
    var status_block = $('#footer .status-block').detach();
    $('section.status .status-row').after(status_block);
}
